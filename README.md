# Simple File Verifier

This tool verifies that two given files are equal. It achieves this by comparing bytes at same locations in given files. If a byte differs, a message will be printed out pointing out which bytes differ. The tool is very simple to use and resembles `dd`. It can be used as a way to verify that image written to a disk has been written properly, hence the name "verifier". The tool was built for personal use but later on shared with the world, meaning some of the features could be better implemented. This is free software, licensed under GPLv3 and contributions to the software are most welcome.

## Usage

```
sfv <fileA> <fileB> [bs=<bs> seek=<seek> count=<count>]
```

Where:

- `<fileA>` - first file (can also be a special file for block devices)
- `<fileB>` - second file (can also be a special file for block devices)
- `<bs>` - block size in bytes, optional argument
- `<seek>` - skip first N bytes from the `<fileA>`, optional argument
- `<count>` - number of bytes to compare, optional argument

**NOTE:** Optional arguments can be in any order whilst files must be set as first two arguments.

**ADDITIONAL NOTE:** This tool is similar to `dd` but it will count in `bytes` not in blocks!

## Exit codes

- If two files are equal, `0` will be returned as exit code.
- If an error occurs during data comparison, `1` will be returned. 
- If `2`, `<fileA>` could not be opened for reading
- If `3`, `<fileB>` could not be opened for reading
- If `4`, files are different in size and hence not equal
- If `5`, memory could not be allocated for the buffers used (no ram?)
- If `6`, given seek is invalid
- If `7`, there was an issue seeking `<fileA>`
- If `8`, there was an issue seeking `<fileB>`
- If `9`, there is no work to be done
