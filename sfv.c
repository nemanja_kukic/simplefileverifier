/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

// Variables

FILE *fileA, *fileB;
char *bufferA, *bufferB;

long blockSize = 64000;
long seek = 0;
long count = 0;

size_t readA;
size_t readB;
size_t globalRead = 0;
size_t fs = 0;

int error = 0;
int countReached = 0;

int64_t tick;

// Just like in Java, return millis since UNIX epoch

int64_t currentTimeMillis() {
  struct timeval time;
  gettimeofday(&time, NULL);
  int64_t s1 = (int64_t)(time.tv_sec) * 1000;
  int64_t s2 = (time.tv_usec / 1000);
  return s1 + s2;
}

int main(int argc, char **argv) {
    
    // Processing arguments
    
    if (argc < 3) {
        fprintf(stderr, "USAGE: %s <fileA> <fileB> [bs=<bs> seek=<seek> count=<count>]\n", argv[0]);
        return 0;
    }

    for (int i = 3; i < argc; ++i) {
        if (!strncmp(argv[i], "bs=", 3)) {
            blockSize = strtol(&argv[i][3], NULL, 10);
            if (blockSize == 0) {
                blockSize = 64000;
            }
        } else if (!strncmp(argv[i], "seek=", 5)) {
            seek = strtol(&argv[i][5], NULL, 10);
        } else if (!strncmp(argv[i], "count=", 6)) {
            count = strtol(&argv[i][6], NULL, 10);
        } else {
            printf("WARNING: Skipping invalid argument: %s\n", argv[i]);
        }
    }
    
    // Open files for reading
    
    fileA = fopen(argv[1], "rb");
    if (fileA == NULL) {
        fprintf(stderr, "ERROR: Failed to open <fileA> for reading!\n");
        return 2;
    }
    
    fileB = fopen(argv[2], "rb");
    if (fileB == NULL) {
        fclose(fileA);
        fprintf(stderr, "ERROR: Failed to open <fileB> for reading!\n");
        return 3;
    }
    
    if (count == 0) {
        // Get file A size
        fseek(fileA, 0L, SEEK_END);
        fs = ftell(fileA);
        rewind(fileA);
        // Check if file B size is equal
        fseek(fileB, 0L, SEEK_END);
        if (fs != ftell(fileB)) {
            fclose(fileA);
            fclose(fileB);
            fprintf(stderr, "ERROR: Files are different in size!\n");
            return 4;
        }
        rewind(fileB);
    } else {
        fs = count;
    }
    
    // Memory allocation
    
    bufferA = malloc(blockSize);
    bufferB = malloc(blockSize);
    
    if (bufferA == NULL || bufferB == NULL) {
        fclose(fileA);
        fclose(fileB);
        fprintf(stderr, "ERROR: Failed to allocate memory for the buffers!\n");
        return 5;
    }
    
    // Seek and count
    
    if (seek > 0) {
        if (seek >= fs) {
            fprintf(stderr, "ERROR: Invalid seek!\n");
            return 6;
        }
        fseek(fileA, seek, SEEK_SET);
        fseek(fileB, seek, SEEK_SET);
        if (ferror(fileA)) {
            fclose(fileA);
            fclose(fileB);
            fprintf(stderr, "ERROR: Failed to seek file A!\n");
            return 7;
        }
        if (ferror(fileB)) {
            fclose(fileA);
            fclose(fileB);
            fprintf(stderr, "ERROR: Failed to seek file B!\n");
            return 8;
        }
        fs -= seek;
        if (fs < 1) {
            fprintf(stderr, "ERROR: No work to be done!\n");
            return 9;
        }
    }
    
    tick = currentTimeMillis();
    
    while ((readA = fread(bufferA, 1, blockSize, fileA)) > 0) {
        if (ferror(fileA)) {
            fprintf(stderr, "%cERROR: Error reading from file A!", globalRead > 0 ? '\n' : '\b');
            error = 1;
            break;
        }
        readB = fread(bufferB, 1, blockSize, fileB);
        if (ferror(fileB)) {
            fprintf(stderr, "%cERROR: Error reading from file B!", globalRead > 0 ? '\n' : '\b');
            error = 1;
            break;
        }
        if (readA != readB) {
            fprintf(stderr, "%cERROR: Couldn't read equal amounts of bytes from files per chunk!", globalRead > 0 ? '\n' : '\b');
            error = 1;
            break;
        }
        for (size_t i = 0; i < readA; ++i) {
            if (bufferA[i] != bufferB[i]) {
                fprintf(stderr, "%cERROR: Bytes at position %zu (absolute: %zu) differ!\n INFO: Got 0x%02x, but expected value is 0x%02x.", globalRead > 0 ? '\n' : '\b', globalRead+i+1, seek+globalRead+i+1, (unsigned char) bufferB[i], (unsigned char) bufferA[i]);
                error = 1;
                break;
            } else {
                if (count > 0) {
                    count -= 1;
                    if (count == 0) {
                        countReached = 1;
                        break;
                    }
                }
            }
        }
        if (countReached == 1) {
            printf("\rVerified: %zu/%zu (%zu%%)", count, count, 100);
            break;
        }
        if (error == 1)
            break;
        globalRead += readA;
        double seconds = (currentTimeMillis() - tick) / 1000.00;
        double bytesPerSecond = globalRead / seconds;
        double kilobytesPerSecond = bytesPerSecond / 1000.00;
        double megabytesPerSecond = kilobytesPerSecond / 1000.00;
        double gigabytesPerSecond = megabytesPerSecond / 1000.00;
        if (gigabytesPerSecond >= 1) {
            printf("\rVerified: %zu/%zu (%zu%%) %.2f GB/s", globalRead, fs, globalRead*100/fs, gigabytesPerSecond);
        } else if (megabytesPerSecond >= 1) {
            printf("\rVerified: %zu/%zu (%zu%%) %.2f MB/s", globalRead, fs, globalRead*100/fs, megabytesPerSecond);
        } else if (kilobytesPerSecond >= 1) {
            printf("\rVerified: %zu/%zu (%zu%%) %.2f kB/s", globalRead, fs, globalRead*100/fs, kilobytesPerSecond);
        } else {
            printf("\rVerified: %zu/%zu (%zu%%) %.2f bytes/s", globalRead, fs, globalRead*100/fs, bytesPerSecond);
        }
        fflush(stdout);
    }
    printf("\n");
    if (ferror(fileA)) {
        fprintf(stderr, "ERROR: Error reading from file A!\n");
        error = 1;
    }
    fclose(fileA);
    fclose(fileB);
    return error;
}
